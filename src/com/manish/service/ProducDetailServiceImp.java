package com.manish.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.manish.dao.ProductDetailDao;
import com.manish.model.ProductDetail;
import com.manish.model.Producttype;

@Service("ProductDetailService")
@Transactional
public class ProducDetailServiceImp implements ProductDetailService {

	private static final String Producttype = null;
	@Autowired
     private ProductDetailDao productDetailDao;

	@Override
	public ProductDetail findproductDetailById(int productDetailById) {

		return productDetailDao.findproductDetailById(productDetailById);

	}

	@Override
	public List<ProductDetail> findAllProductDetail() {

		return productDetailDao.findAllProductDetail();
	}

	@Override
	public void saveProductDetail(ProductDetail productDetail) {
		productDetailDao.saveProductDetail(productDetail);

	}

	@Override
	public void deleteProductDetail(int productDetailId) {
		productDetailDao.deleteProductDetail(productDetailId);

	}

	@Override
	public void updateProductDetail(ProductDetail productDetail) {
		productDetailDao.updateProductDetail(productDetail);
	}

	@Override
	public Producttype findproductDetailTypeBYId(int id) {
		return productDetailDao.findproductDetailTypeBYId(id);
	}

	@Override
	public List<ProductDetail> getAllDetailsByProductType(Producttype product) {
		return productDetailDao.getAllDetailsByProductType( product);
	}

}
