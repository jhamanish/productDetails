package com.manish.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.manish.dao.CategoryDao;
import com.manish.model.Category;

@Service("CategoryService")
@Transactional
public class CategoryServiceImp implements CategoryService 
{

	@Autowired
	private CategoryDao dao;
	@Override
	public Category findByproductCategoryId(int productCategoryId) {
		
		return dao.findByproductCategoryId(productCategoryId);
	}

	@Override
	public List<Category> findAllCategory() {
		
		return dao.findAllCategory();
	}

	@Override
	public void saveCategory(Category category) {
		dao.saveCategory(category);
	}

	@Override
	public void deleteCategory(int productCategoryId) {
		dao.deleteCategory(productCategoryId);
		
	}

	@Override
	public void updateCategory(Category category) {
		dao.updateCategory(category);
	}

}
