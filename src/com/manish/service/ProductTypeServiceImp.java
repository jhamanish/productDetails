package com.manish.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.manish.dao.ProductTypeDao;
import com.manish.model.Producttype;

@Service("ProductTypeService")
@Transactional
public class ProductTypeServiceImp implements ProductTypeService {
	
	@Autowired
	private ProductTypeDao productTypeDao;

	@Override
	public Producttype findByproductTypeId(int productTypeId) {


		return productTypeDao.findByproductTypeId(productTypeId);
	}

	@Override
	public List<Producttype> findAllProductType() {
		
		return productTypeDao.findAllProductType();
	}

	@Override
	public void saveProductType(Producttype productType) {
		
		productTypeDao.saveProductType(productType);
	}

	@Override
	public void deleteProductType(int productTypeId) {
		productTypeDao.deleteProductType(productTypeId);
	}

	@Override
	public void updateProductType(Producttype productType) {
		
		productTypeDao.updateProductType(productType);
		
		
	}

	@Override
	public List<Producttype> findByProductTypeName(String productTypeName) {
		// TODO Auto-generated method stub
		return productTypeDao.findByProductTypeName(productTypeName);
	}


	

}
