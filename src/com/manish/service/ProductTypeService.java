package com.manish.service;

import java.util.List;

import com.manish.model.Producttype;

public interface ProductTypeService {

	public Producttype findByproductTypeId(int productTypeId);

	List<Producttype> findAllProductType();

	public void saveProductType(Producttype productType);

	public void deleteProductType(int productTypeId);

	public void updateProductType(Producttype productType);
	public List<Producttype> findByProductTypeName(String productTypeName);

}
