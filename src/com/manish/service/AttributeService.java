package com.manish.service;

import java.util.List;

import com.manish.model.Attribute;

public interface AttributeService {

	public Attribute findByAttributeId(int attributeId);

	List<Attribute> findAllAttribute();

	public void saveAttribute(Attribute attribute);

	public void deleteAttributeById(Attribute attribute);

	public void updateAttribute(Attribute attribute);

}
