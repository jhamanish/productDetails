package com.manish.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.manish.dao.AttributeDao;
import com.manish.model.Attribute;

@Service("AttributeService")
@Transactional
public class AttributeServiceImp implements AttributeService {

	@Autowired
	AttributeDao attributeDao;

	@Override
	public Attribute findByAttributeId(int attributeId) {

		return attributeDao.findByAttributeId(attributeId);
	}

	@Override
	public List<Attribute> findAllAttribute() {

		return attributeDao.findAllAttribute();
	}

	@Override
	public void saveAttribute(Attribute attribute) {
		attributeDao.saveAttribute(attribute);
	}

	@Override
	public void deleteAttributeById(Attribute attribute) {
		attributeDao.deleteAttributeById(attribute);
	}

	@Override
	public void updateAttribute(Attribute attribute) {
		attributeDao.updateAttribute(attribute);
	}

}
