package com.manish.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manish.model.Attribute;
import com.manish.model.Category;
import com.manish.service.AttributeService;

@RestController
public class AttributeTypeRestController {

	@Autowired
	AttributeService  attributeService;

	// -----------------------create attribute type--------------------

	@RequestMapping(value = "/attribute/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Attribute> createAttribute(@RequestBody Attribute attribute) {
		
		Attribute attribute1 =new Attribute();
		attribute1.setAttributes(attribute.getAttributes());
		attribute1.setValue(attribute.getValue());
		attribute1.setAttributeDisplayName(attribute.getAttributeDisplayName());
		attributeService.saveAttribute(attribute1);
		return new ResponseEntity<Attribute>(attribute1, HttpStatus.OK);

	}
	
	//------------------- Retrieve single attribute 
	

	@RequestMapping(value = "/attribute/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Attribute> getAttribute(@PathVariable("id") int id) {
		System.out.println("Fetching_category with product_category_id" + id);
		Attribute attribute=attributeService.findByAttributeId(id);
		if (attribute == null) {
			System.out.println("not faound");
			return new ResponseEntity<Attribute>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Attribute>(attribute, HttpStatus.OK);

	}
	
	// ----------------Retrieve all attribute-----------------------

	@RequestMapping(value = "/attribute/", method = RequestMethod.GET)
	public ResponseEntity<List<Attribute>> listAllAttribute() {

		List<Attribute> attribute = attributeService.findAllAttribute();
		if (attribute.isEmpty()) {
			return new ResponseEntity<List<Attribute>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Attribute>>(attribute, HttpStatus.OK);

	}
	
//------------------------delete single  attribute
	@RequestMapping(value = "/attribute/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Attribute> deleteAttribute(@PathVariable("id") int id) {
		System.out.println("Fetching & Deleting User with id " + id);

	Attribute attribute= attributeService.findByAttributeId(id);
		if (attribute == null) {
			System.out.println("Unable to delete. Category with product_category_id " + id + " not found");
			return new ResponseEntity<Attribute>(HttpStatus.NOT_FOUND);
		}

		attributeService.deleteAttributeById(attribute);
		return new ResponseEntity<Attribute>(HttpStatus.NO_CONTENT);
	}

	// ----------------------update---------

		@RequestMapping(value = "/attribute/{id}", method = RequestMethod.PUT)
		public ResponseEntity<Attribute> updateAttribute(@PathVariable("id") int id, @RequestBody Attribute attribute) {
			System.out.println("Updating Category " + id);

		Attribute attribute1 = attributeService.findByAttributeId(id);

			if (attribute == null) {
				System.out.println("attribute with id " + id + " not found");
				return new ResponseEntity<Attribute>(HttpStatus.NOT_FOUND);
			}

			attribute1.setAttributes(attribute.getAttributes());

			attributeService.updateAttribute(attribute1);
			
			return new ResponseEntity<Attribute>(attribute1, HttpStatus.OK);
		}
	
}
