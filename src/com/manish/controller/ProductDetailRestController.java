package com.manish.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manish.model.Attribute;
import com.manish.model.ProductDetail;
import com.manish.model.Producttype;
import com.manish.service.AttributeService;
import com.manish.service.ProductDetailService;
import com.manish.service.ProductTypeService;

@RestController
public class ProductDetailRestController {
	
	@Autowired
	ProductDetailService productDetailService;
	@Autowired
	ProductTypeService productTypeService;
	@Autowired
	AttributeService attributeService;
	
	
	

	// ----------------Retrieve all productType-----------------------

	@RequestMapping(value = "/productdetail/", method = RequestMethod.GET)
	public ResponseEntity<List<ProductDetail>> listAllCategory() {

		List<ProductDetail> productDeatil = productDetailService.findAllProductDetail();
		if (productDeatil.isEmpty()) {
			return new ResponseEntity<List<ProductDetail>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<ProductDetail>>(productDeatil, HttpStatus.OK);

	}
	
	
	// -----------------------Retrieve single productDetail--------------------

			@RequestMapping(value = "/productdetail/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
			public ResponseEntity<ProductDetail> getProductDetail(@PathVariable("id") int id) {
				System.out.println("Fetching_productDetail with product_detail_id" + id);
			ProductDetail productDetail = productDetailService.findproductDetailById(id);

				if (productDetail == null) {
					System.out.println("not faound");
					return new ResponseEntity<ProductDetail>(HttpStatus.NOT_FOUND);
				}
				return new ResponseEntity<ProductDetail>(productDetail, HttpStatus.OK);

			}
			
			// -----------------------create product detail--------------------

			@RequestMapping(value = "/productdetail/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
			public ResponseEntity<ProductDetail> createProductDetail(@RequestBody ProductDetail productDetail) {
				//find productType id 
				
				System.out.println("ssssssssssssssss");
				int productTypeId=productDetail.getProducttype().getProductTypeId();
				
				System.out.println(productTypeId);
				//find product
				Producttype productType=productTypeService.findByproductTypeId(productTypeId);
				//	find	attribute id
				int attributeId=productDetail.getAttribute().getAttrubuteId();
				System.out.println("attribute id"+ attributeId);
				Attribute attribute=attributeService.findByAttributeId(attributeId);
				ProductDetail productDetail2=new ProductDetail();
				productDetail2.setAttribute(attribute);
				productDetail2.setProducttype(productType);
				productDetailService.saveProductDetail(productDetail2);

				return new ResponseEntity<ProductDetail>(productDetail2, HttpStatus.OK);

			}
			
			
			//-----------------update product detail------------
			
			@RequestMapping(value = "/productdetail/{id}", method = RequestMethod.PUT,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
		    public ResponseEntity<ProductDetail> updateDetail(@PathVariable("id") int id, @RequestBody ProductDetail productDetail) {
		        System.out.println("Updating Category " + id);
		       ProductDetail productDetail1= 	productDetailService.findproductDetailById(id);
		    
		       if (productDetail1==null) {
		            System.out.println("product detail with id " + id + " not found");
		            return new ResponseEntity<ProductDetail>(HttpStatus.NOT_FOUND);
		        }
		 
		        productDetail1.setAttribute(productDetail.getAttribute());
		        productDetail1.setProducttype(productDetail.getProducttype());
		        productDetailService.updateProductDetail(productDetail1);
		        return new ResponseEntity<ProductDetail>(productDetail1, HttpStatus.OK);
		    }
			
			
			

			// -------------------------delete product detail-------

			@RequestMapping(value = "/productdetail/{id}", method = RequestMethod.DELETE)
			public ResponseEntity<Producttype> deleteProductType(@PathVariable("id") int id) {
				System.out.println("Fetching & Deleting producttype with id " + id);

				ProductDetail productDetail =productDetailService.findproductDetailById(id);
				if (productDetail == null) {
					System.out.println("Unable to delete. product Detail with product_Detail_id " + id + " not found");
					return new ResponseEntity<Producttype>(HttpStatus.NOT_FOUND);
				}
			productDetailService.deleteProductDetail(id);

				return new ResponseEntity<Producttype>(HttpStatus.NO_CONTENT);
			}
			
			@RequestMapping(value = "/productdetailtype/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
			public ResponseEntity<Producttype> getProductDetailtype(@PathVariable("id") int id) {
				System.out.println("Fetching_productDetail with product_detail_id" + id);
			Producttype producttype = productDetailService.findproductDetailTypeBYId(id);

				if (producttype ==null) {
					System.out.println("not faound");
					return new ResponseEntity<Producttype>(HttpStatus.NOT_FOUND);
				}
				return new ResponseEntity<Producttype>(producttype, HttpStatus.OK);

			}
			
			@RequestMapping(value="/getProductDetails/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
			 public ResponseEntity<List<ProductDetail>> getProductDetails(@PathVariable("id") Integer id){
			  Producttype product = productTypeService.findByproductTypeId(id);
			  List<ProductDetail> allProductDetails = productDetailService.getAllDetailsByProductType(product);
			  return new ResponseEntity<List<ProductDetail>>(allProductDetails, HttpStatus.OK);
			 }
			
			
			
}
