package com.manish.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manish.model.Category;
import com.manish.model.Producttype;
import com.manish.service.CategoryService;
import com.manish.service.ProductTypeService;

@RestController
public class ProductTypeRestController {

	@Autowired
	ProductTypeService productTypeService;
	@Autowired
	CategoryService categoryService;

	// ----------------Retrieve all productType-----------------------

	@RequestMapping(value = "/producttype/", method = RequestMethod.GET)
	public ResponseEntity<List<Producttype>> listAllCategory() {

		List<Producttype> productTypes = productTypeService.findAllProductType();
		if (productTypes.isEmpty()) {
			return new ResponseEntity<List<Producttype>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Producttype>>(productTypes, HttpStatus.OK);

	}

	// -----------------------Retrieve single Category--------------------

	@RequestMapping(value = "/producttype/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producttype> getCategory(@PathVariable("id") int id) {
		System.out.println("Fetching_category with product_category_id" + id);
		Producttype productType = productTypeService.findByproductTypeId(id);

		if (productType == null) {
			System.out.println("not faound");
			return new ResponseEntity<Producttype>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Producttype>(productType, HttpStatus.OK);

	}

	// -----------------------create product type--------------------

	@RequestMapping(value = "/producttype/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producttype> createProductType(@RequestBody Producttype productType) {
		int id = productType.getCategory().getProductCatagoryId();
		Category category = categoryService.findByproductCategoryId(id);
		Producttype productType1 = new Producttype();
		productType1.setCategory(category);
		productType1.setProductType(productType.getProductType());
		productType1.setProductDisplayName(productType.getProductDisplayName());
		productTypeService.saveProductType(productType1);

		return new ResponseEntity<Producttype>(productType1, HttpStatus.OK);

	}

	// -------------------------delete product type-------

	@RequestMapping(value = "/producttype/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Producttype> deleteProductType(@PathVariable("id") int id) {
		System.out.println("Fetching & Deleting producttype with id " + id);

		Producttype productType = productTypeService.findByproductTypeId(id);
		if (productType == null) {
			System.out.println("Unable to delete. product typen with product_type_id " + id + " not found");
			return new ResponseEntity<Producttype>(HttpStatus.NOT_FOUND);
		}
		productTypeService.deleteProductType(id);

		return new ResponseEntity<Producttype>(HttpStatus.NO_CONTENT);
	}
	// -----------------update product type ----------------------

	@RequestMapping(value = "/producttype/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producttype> updateType(@PathVariable("id") int id, @RequestBody Producttype producttype) {
		System.out.println("Updating Category " + id);
		Producttype producttype2 = productTypeService.findByproductTypeId(id);

		if (producttype2 == null) {
			System.out.println("category with id " + id + " not found");
			return new ResponseEntity<Producttype>(HttpStatus.NOT_FOUND);
		}

		producttype2.setProductType(producttype.getProductType());

		productTypeService.updateProductType(producttype2);
		
		return new ResponseEntity<Producttype>(producttype2, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/producttypebyname/{productname}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Producttype>> findByName(@PathVariable("productname") String productname) {
		List<Producttype> productType = productTypeService.findByProductTypeName(productname);

		if (productType.isEmpty()) {
			System.out.println("not faound");
			return new ResponseEntity<List<Producttype>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Producttype>>(productType, HttpStatus.OK);

	
		
	}
}
