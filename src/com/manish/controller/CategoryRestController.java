package com.manish.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manish.model.Category;
import com.manish.service.CategoryService;

@RestController
public class CategoryRestController {
	@Autowired
	CategoryService categoryService;

	// ----------------Retrieve all Category-----------------------

	@RequestMapping(value = "/category/", method = RequestMethod.GET)
	public ResponseEntity<List<Category>> listAllCategory() {

		List<Category> categories = categoryService.findAllCategory();
		if (categories.isEmpty()) {
			return new ResponseEntity<List<Category>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Category>>(categories, HttpStatus.OK);

	}

	// -----------------------Retrieve single Category--------------------

	@RequestMapping(value = "/category/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Category> getCategory(@PathVariable("id") int id) {
		System.out.println("Fetching_category with product_category_id" + id);
		Category category = categoryService.findByproductCategoryId(id);

		if (category == null) {
			System.out.println("not faound");
			return new ResponseEntity<Category>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Category>(category, HttpStatus.OK);

	}

	// -----------------------create Category--------------------

	@RequestMapping(value = "/category/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Category> createCategory(@RequestBody Category category) {
		Category category1 = new Category();
		category1.setProductCategory(category.getProductCategory());
		categoryService.saveCategory(category1);

		return new ResponseEntity<Category>(category1, HttpStatus.OK);

	}

	// -------------------------delete category-------

	@RequestMapping(value = "/category/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Category> deleteCategory(@PathVariable("id") int id) {
		System.out.println("Fetching & Deleting User with id " + id);

		Category category = categoryService.findByproductCategoryId(id);
		if (category == null) {
			System.out.println("Unable to delete. Category with product_category_id " + id + " not found");
			return new ResponseEntity<Category>(HttpStatus.NOT_FOUND);
		}

		categoryService.deleteCategory(id);
		return new ResponseEntity<Category>(HttpStatus.NO_CONTENT);
	}

	// ----------------------update---------

	@RequestMapping(value = "/category/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Category> updateCategory(@PathVariable("id") int id, @RequestBody Category category) {
		System.out.println("Updating Category " + id);

		Category category2 = categoryService.findByproductCategoryId(id);

		if (category2 == null) {
			System.out.println("category with id " + id + " not found");
			return new ResponseEntity<Category>(HttpStatus.NOT_FOUND);
		}

		category2.setProductCategory(category.getProductCategory());

		categoryService.updateCategory(category2);
		return new ResponseEntity<Category>(category2, HttpStatus.OK);
	}

}
