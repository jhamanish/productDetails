package com.manish.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.manish.model.Attribute;
import com.manish.service.AttributeService;

@Repository("AttributeDao")
public class AttributeDaoImp extends AbstractDao<Integer, Attribute> implements AttributeDao {

	@Autowired
	AttributeService attributeService; 
	@Override
	public Attribute findByAttributeId(int attributeId) {
		return getByKey(attributeId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Attribute> findAllAttribute() {
		Criteria criteria=createEntityCriteria();
		return (List<Attribute>)criteria.list();
	}

	@Override
	public void saveAttribute(Attribute attribute) {
		persist(attribute);
	}

	@Override
	public void deleteAttributeById(Attribute attribute) {
	
		delete(attribute);

		
	}

	@Override
	public void updateAttribute(Attribute attribute) {
		update(attribute);
	}
	

}
