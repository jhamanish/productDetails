
package com.manish.dao;

import java.util.List;

import com.manish.model.Category;

public interface CategoryDao
{	
	 public  Category findByproductCategoryId(int productCategoryId);

	 List<Category> findAllCategory();
	 public void saveCategory(Category category);
	 public void deleteCategory(int productCategoryId);
	 public void updateCategory(Category category);

}
