package com.manish.dao;

import java.util.List;

import com.manish.model.ProductDetail;
import com.manish.model.Producttype;

public interface ProductDetailDao {
	public ProductDetail findproductDetailById(int productDetailById);

	List<ProductDetail> findAllProductDetail();

	public void saveProductDetail(ProductDetail productDetail);

	public void deleteProductDetail(int productDetailId);

	public void updateProductDetail(ProductDetail productDetail);

	public Producttype findproductDetailTypeBYId(int id);

	public List<ProductDetail> getAllDetailsByProductType(Producttype product);
}
