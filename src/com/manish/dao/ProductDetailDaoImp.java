package com.manish.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.manish.model.ProductDetail;
import com.manish.model.Producttype;
import com.manish.service.ProductDetailService;
import com.manish.service.ProductTypeService;
@Repository("ProductDetailDao")
public class ProductDetailDaoImp extends AbstractDao<Integer, ProductDetail> implements ProductDetailDao{

	@Autowired
	ProductDetailService productDetailService; 
	
	@Autowired
	ProductTypeService productTypeService;
	
	
	@Override
	public ProductDetail findproductDetailById(int productDetailById) {
		
		return getByKey(productDetailById);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductDetail> findAllProductDetail() {
		Criteria criteria = createEntityCriteria();
		return (List<ProductDetail>) criteria.list();
		
	}

	@Override
	public void saveProductDetail(ProductDetail productDetail) {
		persist(productDetail);
	}

	@Override
	public void deleteProductDetail(int productDetailId) {
		ProductDetail productDetail=productDetailService.findproductDetailById(productDetailId); 
		delete(productDetail);
	}

	@Override
	public void updateProductDetail(ProductDetail productDetail) {
		update(productDetail);
		
	}

	@Override
	public Producttype findproductDetailTypeBYId(int id) {
		return productTypeService.findByproductTypeId(id) ;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductDetail> getAllDetailsByProductType(Producttype producttype) {
		Criteria criteria=createEntityCriteria();
		criteria.add(Restrictions.eq("producttype", producttype));
		return (List<ProductDetail>) criteria.list();
	}

}
