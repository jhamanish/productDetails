package com.manish.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.manish.model.Category;
import com.manish.service.CategoryService;

@Repository("CategoryDao")
public class CategoryDaoImp extends AbstractDao<Integer, Category> implements CategoryDao {
	@Autowired
	CategoryService categoryService;

	@Override
	public Category findByproductCategoryId(int productCategoryId) {

		return getByKey(productCategoryId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> findAllCategory() {
		Criteria criteria = createEntityCriteria();
		return (List<Category>) criteria.list();

	}

	@Override
	public void saveCategory(Category category) {
		persist(category);

	}

	@Override
	public void deleteCategory(int productCategoryId) {
		
		Category category = categoryService.findByproductCategoryId(productCategoryId);
		delete(category);

	}

	@Override
	public void updateCategory(Category category) {
		System.out.println(category.getProductCategory());
		update(category);
	}

}
