package com.manish.dao;

import java.util.List;

import com.manish.model.Producttype;

public interface ProductTypeDao {
	public Producttype findByproductTypeId(int productTypeId);

	List<Producttype> findAllProductType();

	public void saveProductType(Producttype productType);

	public void deleteProductType(int productTypeId);

	public void updateProductType(Producttype productType);
	
	public List<Producttype> findByProductTypeName(String productTypeName);

	
}
