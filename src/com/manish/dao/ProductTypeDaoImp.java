/**
 * 
 */
package com.manish.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.manish.model.Producttype;
import com.manish.service.ProductTypeService;

/**
 * @author manish
 *
 */

@Repository("ProductTypeDao")
public class ProductTypeDaoImp extends AbstractDao<Integer, Producttype> implements ProductTypeDao {
	
	@Autowired
	ProductTypeService productTypeService;
	
	@Override
	public Producttype findByproductTypeId(int productTypeId) {
		return getByKey(productTypeId);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Producttype> findAllProductType() {
		Criteria criteria = createEntityCriteria();
		return (List<Producttype>) criteria.list();

	}

	@Override
	public void saveProductType(Producttype productType) {
		persist(productType);
	}

	@Override
	public void deleteProductType(int productTypeId) {
	//	Query query = getSession().createSQLQuery("delete from ProductType where product_type_id =" + productTypeId);

		//query.executeUpdate();
		Producttype producttype=productTypeService.findByproductTypeId(productTypeId);
		delete(producttype);
	}

	@Override
	public void updateProductType(Producttype productType) {
		update(productType);
		//return updateProductType(productType);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Producttype> findByProductTypeName(String productTypeName) {
		Criteria criteria=createEntityCriteria();
		criteria.add(Restrictions.ilike("productDisplayName", productTypeName,MatchMode.ANYWHERE));
			
		return (List<Producttype>) criteria.list();
	}

	
	

}
