package com.manish.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the category database table.
 * 
 */
@Entity

public class Category implements Serializable {
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="product_catagory_id")
	private int productCatagoryId;

	@Column(name="product_category")
	private String productCategory;

	//bi-directional many-to-one association to Producttype
	//	@OneToMany(mappedBy="category",fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JsonIgnore
	@OneToMany(mappedBy="category",cascade=CascadeType.ALL,orphanRemoval=true)
	//@NotFound(action=NotFoundAction.IGNORE)
	@LazyCollection(LazyCollectionOption.FALSE)
	@Fetch(value=FetchMode.SUBSELECT)
	private List<Producttype> producttypes;

	public Category() {
	}

	public int getProductCatagoryId() {
		return this.productCatagoryId;
	}

	public void setProductCatagoryId(int productCatagoryId) {
		this.productCatagoryId = productCatagoryId;
	}

	public String getProductCategory() {
		return this.productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public List<Producttype> getProducttypes() {
		return this.producttypes;
	}

	public void setProducttypes(List<Producttype> producttypes) {
		this.producttypes = producttypes;
	}

	/*public Producttype addProducttype(Producttype producttype) {
		getProducttypes().add(producttype);
		producttype.setCategory(this);

		return producttype;
	}

	public Producttype removeProducttype(Producttype producttype) {
		getProducttypes().remove(producttype);
		producttype.setCategory(null);

		return producttype;
	}*/

}